# Ülesanne

Loo Laravel rakendus, mis sisaldab andmebaasi ja puuvilju.

See tähendab, et kontrolleris (CRUD) peavad olema mõned tegevused:

- Vaadata kõiki puuvilju, mis on baasis
- Vaadata konkreetset puuvilja
- Muuta puuvilja
- Lisada puuvilja
- Kustutada puuvilja
- Puuviljal on mõned väljad, näiteks pealkiri, kirjeldus.

Testid on teretulnud, kuid pole kohustuslikud.

# Demo

## Lähtekood

https://gitlab.com/raido.orumets/demo-fruit-app

## Avalik veeb

Demo asub aadressil: https://fruit-app.orumets.ee

## Admin

Admin demo asub aadressil: https://fruit-app.orumets.ee/admin

### Admin:

- Kasutaja: **admin@orumets.ee**
- Parool: **DemoFruitApp**

### Tavakasutaja:

- Kasutaja: **user@orumets.ee**
- Parool: **DemoFruitApp**

# Install

Tõmba alla repo:

```sh
$ git clone https://gitlab.com/raido.orumets/demo-fruit-app.git
$ cd demo-fruit-app
```

Installeeri paketid ja vajalikud failid:

```sh
$ composer install
$ php -r "file_exists('.env') || copy('.env.example', '.env');"
$ php artisan key:generate --ansi
```
Loo andmebaas ja seadista ".env" fail:

```sh
APP_URL=http://127.0.0.1:8000

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=8889
DB_DATABASE=fruit_app_orumets_ee
DB_USERNAME=root
DB_PASSWORD=root
```

Loo baasi tabelid ja sööda neisse andmed:

```sh
php artisan migrate --seed
```

Installeeri kasutajaliidese failid:
```sh
$ yarn
$ yarn run dev
```


Serveeri veebiaplikatsioon:

```sh
php artisan serve
```