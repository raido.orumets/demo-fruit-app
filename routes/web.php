<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FruitController;
use App\Http\Controllers\Admin\FruitController as AdminFruitController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

//Visitor
Route::get('/', [FruitController::class, 'index'])->name('fruits.index');
Route::get('dashboard', function () {
    return redirect()->route('dashboard');
});

Route::prefix('admin')->group(function () {
    // Visitor
    Route::middleware(['guest'])->group(function () {
        Route::get('/', function () {
            return redirect()->route('login');
        });
    });

    // User
    Route::middleware(['auth'])->group(function () {
        Route::resource('fruits', AdminFruitController::class);
        Route::view('dashboard', 'dashboard')->name('dashboard');
        Route::get('/', function () {
            return redirect()->route('dashboard');
        });
    });

    // Admin
    Route::middleware(['auth', 'admin'])->group(function () {
        Route::redirect('/', route('fruits.index'));
        Route::resource('fruits', AdminFruitController::class, ['as' => 'admin'])->except(['show']);
    });
});
