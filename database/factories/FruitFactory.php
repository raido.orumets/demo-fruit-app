<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FruitFactory extends Factory
{
    public function definition()
    {
        return [
            'name' => Str::ucfirst($this->faker->word()),
            'description' => $this->faker->text,
            'image' => null,
            'image_url' => $this->fruitImageUrl(640, 480), //$this->faker->imageUrl(640, 480, 'fruits'),
        ];
    }

    private function fruitImageUrl($width, $height)
    {
        return "http://loremflickr.com/{$width}/{$height}/fruit?" . Str::random(5);
    }
}
