<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(['email' => 'admin@orumets.ee'], [
            'name' => 'Admin',
            'is_admin' => true,
            'password' => bcrypt('DemoFruitApp'),
            'email_verified_at' => now(),
        ]);

        User::updateOrCreate(['email' => 'user@orumets.ee'], [
            'name' => 'User',
            'is_admin' => false,
            'password' => bcrypt('DemoFruitApp'),
            'email_verified_at' => now(),
        ]);
    }
}
