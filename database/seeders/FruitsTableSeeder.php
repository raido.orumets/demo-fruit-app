<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Fruit;

class FruitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fruit::factory(200)->create();
    }
}
