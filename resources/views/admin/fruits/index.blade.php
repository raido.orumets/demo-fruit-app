<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Fruits') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <!-- Session Status -->
                    <x-session-status class="mb-4" :status="session('status')" />

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    
                    <div class="flex items-center justify-end mb-4">
                        <x-button class="ml-4" onclick="location.href='{{ route('admin.fruits.create') }}'">
                            {{ __('Add') }}
                        </x-button>
                    </div>

                    <!-- Table -->
                    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mb-5">
                        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                <tr>
                                    <th scope="col" class="px-6 py-3">
                                        ID
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Name
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Image
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Description
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        <span class="sr-only">Delete</span>
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        <span class="sr-only">Edit</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <!-- Table:row -->
                                @foreach($fruits as $fruit)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="px-6 py-4 font-medium text-bold text-slate-300 dark:text-white whitespace-nowrap">
                                        #{{ $fruit->id }}
                                    </th>
                                    <td class="px-6 py-4">
                                        {{ $fruit->name }}
                                    </th>
                                    <td class="px-6 py-4">
                                        <img src="{{ $fruit->image_url }}" class="rounded w-12 h-12" alt="{{ $fruit->name }}" />
                                        </th>
                                    <td class="px-6 py-4">
                                        {{ $fruit->description }}
                                    </td>
                                    <td class="px-6 py-4 text-right">
                                        <form action="{{ route('admin.fruits.destroy', $fruit) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <x-button type="submit" class="font-medium text-red-600 dark:text-red-500 bg-slate-100 hover:bg-slate-200 normal-case" onclick="return confirm('Delete fruit?');">
                                                {{ __('Delete') }}
                                            </x-button>
                                        </form>
                                    </td>
                                    <td class="px-6 py-4 text-right">
                                        <a href="{{ route('admin.fruits.edit', $fruit) }}" class="font-medium text-violet-600 dark:text-violet-500 hover:underline">{{ __('Edit') }}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {!! $fruits->links()!!}

                </div>
            </div>
        </div>
    </div>
</x-app-layout>