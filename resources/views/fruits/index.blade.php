<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Fruits') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="p-3 grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-4 gap-5">
                        @foreach($fruits as $fruit)
                        <x-product-card :name="$fruit->name" :description="$fruit->description" :image="$fruit->image_url" :url="route('fruits.index', $fruit)" />
                        @endforeach
                    </div>

                    <div class="p-3">
                        {!! $fruits->links() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>