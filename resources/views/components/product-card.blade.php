@props(['name', 'description', 'image', 'url'])

<div class="rounded overflow-hidden shadow-lg">
    <img class="w-full" src="{{ $image }}" alt="Mountain">
    <div class="px-6 py-4">
        <div class="font-bold text-xl mb-2"><a href="{{ $url }}">{{ $name }}</a></div>
        <p class="text-gray-700 text-base">
            {{ $description ?? $slot }}
        </p>
    </div>
</div>