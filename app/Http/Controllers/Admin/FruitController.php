<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Fruit;
use App\Http\Requests\StoreFruitRequest;
use App\Http\Requests\UpdateFruitRequest;

class FruitController extends Controller
{
    public function index()
    {
        $fruits = Fruit::orderBy('id', 'desc')->paginate(12);

        return view('admin.fruits.index', ['fruits' => $fruits]);
    }

    public function create()
    {
        return view('admin.fruits.create');
    }

    public function store(StoreFruitRequest $request)
    {
        Fruit::create($request->validated());
         
        return redirect()
                    ->route('admin.fruits.index')
                    ->withStatus('Fruit added successfully!');
    }

    public function edit(Fruit $fruit)
    {
        return view('admin.fruits.edit', ['fruit' => $fruit]);
    }

    public function update(UpdateFruitRequest $request, Fruit $fruit)
    {
        $fruit->update($request->validated());
        
        return redirect()
                    ->back()
                    ->withStatus('Fruit update successfully!');
    }

    public function destroy(Fruit $fruit)
    {
        $fruit->delete();

        return redirect()
                    ->back()
                    ->withStatus('Fruit deleted successfully!');
    }
}
