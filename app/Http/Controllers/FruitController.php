<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fruit;

class FruitController extends Controller
{
    public function index()
    {
        $fruits = Fruit::orderBy('id', 'desc')->paginate(12);

        return view('fruits.index', [
            'fruits' => $fruits
        ]);
    }
}
